<?php


return [
    'phrases' => 'Phrases',
    'create_new_phrase' => 'Create new phrase',
    'save_phrase' => 'Save phrase',
    'phrase' => 'Phrase',
    'russian' => 'Translate into Russian',
    'english' => 'Translate into English',
    'mongol' => 'Translate into Mongol',
    'china'  => 'Translate into China',
    'tatar' => 'Translate into Tatar',
    'translate_ru' => 'This phrase has been translated into Russian',
    'translate_en' => 'This phrase has been translated into English',
    'translate_mn' => 'This phrase has been translated into Mongol',
    'translate_zh' => 'This phrase has been translated into China',
    'translate_tt' => 'This phrase has been translated into Tatar',
    'login' => 'Login',
    'register' => 'Register',
    'can_translate' => 'Please translate',
    'logout' => 'Logout',
    'confirm_password' => 'Confirm Password',
    'password_before' => 'Please confirm your password before continuing.',
    'password' => 'Password',
    'forgot' => 'Forgot Your Password?',
    'reset_password' => 'Reset Password',
    'email_address' => 'Email Address',
    'send_password' => 'Send Password Reset Link',
    'remember_me' => 'Remember Me',
    'name' => 'Name',
    'verify_email_address' => 'Verify Your Email Address',
    'fresh_verification_email_address' => 'A fresh verification link has been sent to your email address.',
    'before_proceeding' => 'Before proceeding, please check your email for a verification link.',
    'receive_email' => 'If you did not receive the email',
    'click_here' => 'click here to request another',

];


