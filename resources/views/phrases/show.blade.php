@extends('layouts.app')
@section('content')
    <div class="container">
        <dv>
            <h3>@lang('messages.can_translate') "{{$phrase->translate('ru')->phrase}}"</h3>
        </dv>


        @if(Auth::check())
            <form method="post" action="{{route('phrases.update', ['phrase' => $phrase])}}">
                @method('put')
                @csrf
                <input type="hidden" id="phrase_id" value="{{$phrase->id}}">
                @if(!empty($phrase->translate('ru')->phrase))
                    <h4>{{$phrase->translate('ru')->phrase}}</h4>
                @else
                    <div class="form-group">
                        <label for="ru_phrase">@lang('messages.russian')</label>
                        <input name="ru_phrase"  type="text" class="form-control"
                               id="ru_phrase">
                    </div>
                @endif
                @if(!empty($phrase->translate('en')->phrase))
                    <h4>{{$phrase->translate('en')->phrase}}</h4>
                @else
                    <div class="form-group">
                        <label for="en_phrase">@lang('messages.english')</label>
                        <input name="en_phrase"  type="text" class="form-control"
                               id="en_phrase">
                    </div>
                @endif
                @if(!empty($phrase->translate('mn')->phrase))
                    <h4>{{$phrase->translate('mn')->phrase}}</h4>
                @else
                    <div class="form-group">
                        <label for="mn_phrase">@lang('messages.mongol')</label>
                        <input name="mn_phrase" type="text" class="form-control"
                               id="mn_phrase">
                    </div>
                @endif
                @if(!empty($phrase->translate('zh')->phrase))
                    <h4>{{$phrase->translate('zh')->phrase}}</h4>
                @else
                    <div class="form-group">
                        <label for="zh_phrase">@lang('messages.china')</label>
                        <input name="zh_phrase" type="text" class="form-control"
                               id="zh_phrase" >
                    </div>
                @endif
                @if(!empty($phrase->translate('tt')->phrase))
                    <h4>{{$phrase->translate('tt')->phrase}}</h4>
                @else
                    <div class="form-group">
                        <label for="tt_phrase">@lang('messages.tatar')</label>
                        <input name="tt_phrase" type="text" class="form-control"
                               id="tt_phrase" >
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">@lang('messages.save_phrase')</button>
            </form>
        @else
            @if(!empty($phrase->translate('ru')->phrase))
                <div>
                    <b>@lang('messages.translate_ru')</b><br>
                    <h5>
                        {{$phrase->translate('ru')->phrase}}
                    </h5>
                </div>
            @endif

            @if(!empty($phrase->translate('en')->phrase))
                <div>
                    <b>@lang('messages.translate_en')</b><br>
                    <h5>
                        {{$phrase->translate('en')->phrase}}
                    </h5>
                </div>
            @endif

            @if(!empty($phrase->translate('mn')->phrase))
                <div>
                    <b>@lang('messages.translate_mn')</b><br>
                    <h5>
                        {{$phrase->translate('mn')->phrase}}
                    </h5>
                </div>
            @endif
            @if(!empty($phrase->translate('zh')->phrase))
                <div>
                    <b>@lang('messages.translate_zh')</b><br>
                    <h5>
                        {{$phrase->translate('zh')->phrase}}
                    </h5>
                </div>
            @endif
            @if(!empty($phrase->translate('tt')->phrase))
                <div>
                    <b>@lang('messages.translate_tt')</b><br>
                    <h5>
                        {{$phrase->translate('tt')->phrase}}
                    </h5>
                </div>
            @endif
        @endif
    </div>

@endsection
