@extends('layouts.app')
@section('content')
    <div class="container" >
        <h3>
            @lang('messages.phrases')
        </h3>
        <div>
            @foreach($phrases as $phrase)
                <h5><a href="{{route('phrases.show', ['phrase' => $phrase])}}">{{$phrase->translate('ru')->phrase}}</a></h5>
            @endforeach
        </div>
            @if(Auth::check())
                <div>
                    <h5>
                        @lang('messages.create_new_phrase')
                    </h5>

                    <form method="post" action="{{route('phrases.store')}}">
                        @csrf

                        <div class="form-group mb-3">
                            <label for="ru_phrase">@lang('messages.phrase')</label>
                            <input name="ru_phrase" type="text" class="form-control border-success  @error('phrase') is-invalid border-danger @enderror"
                                   id="ru_phrase">
                        </div>
                        @error('phrase')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        <button type="submit" class="btn btn-primary">@lang('messages.save_phrase')</button>
                    </form>
                </div>
            @endif
    </div>

@endsection
