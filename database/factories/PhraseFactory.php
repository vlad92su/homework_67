<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Phrase>
 */
class PhraseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $ru_faker = \Faker\Factory::create('ru_RU');
        $mn_faker = \Faker\Factory::create('mn_MN');
        $zh_faker = \Faker\Factory::create('zh_ZH');
        $tt_faker = \Faker\Factory::create('tt_TT');

        return [
            'ru' => [
                'phrase' => 'RU - '. $ru_faker->sentence(),
            ],
            'en' => [
                'phrase' => 'EN - '. $this->faker->sentence(),
            ],
            'mn' => [
                'phrase' =>  'MN - '.$mn_faker->sentence(),
            ],
            'zh' => [
                'phrase' =>  'ZH - '.$zh_faker->sentence(),
            ],
            'tt' => [
                'phrase' =>  'TT - '. $tt_faker->sentence(),
            ]
        ];

    }
}
