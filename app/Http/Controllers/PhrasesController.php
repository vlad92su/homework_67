<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhraseRequest;
use App\Models\Phrase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PhrasesController extends Controller
{

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        $phrases = Phrase::all();
        return view('phrases.index', compact('phrases'));
    }


    /**
     * @param PhraseRequest $request
     * @return RedirectResponse
     */
    public function store(PhraseRequest $request)
    {
        $phrase = new Phrase();
        $data = [
            'ru' =>
                [
                    'phrase' => $request->input('ru_phrase'),
                ]
        ];

        $phrase->create($data);
        return back();

    }


    /**
     * @param Phrase $phrase
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function show(Phrase $phrase)
    {
        return view('phrases.show', compact('phrase'));
    }


    /**
     * @param Request $request
     * @param Phrase $phrase
     * @return RedirectResponse
     */
    public function update(Request $request, Phrase $phrase)
    {
        if(!empty($request->input('en_phrase'))) {
            $data = [
                'en' =>
                    [
                        'phrase' => $request->input('en_phrase'),
                    ]
            ];
            $phrase->update($data);
        }
        if(!empty($request->input('mn_phrase'))) {
            $data = [
                'mn' =>
                    [
                        'phrase' => $request->input('mn_phrase'),
                    ]
            ];
            $phrase->update($data);
        }
        if(!empty($request->input('zh_phrase'))) {
            $data = [
                'zh' =>
                    [
                        'phrase' => $request->input('zh_phrase'),
                    ]
            ];
            $phrase->update($data);
        }
        if(!empty($request->input('tt_phrase'))) {
            $data = [
                'tt' =>
                    [
                        'phrase' => $request->input('tt_phrase'),
                    ]
            ];
            $phrase->update($data);
        }

        return back();
    }
}
